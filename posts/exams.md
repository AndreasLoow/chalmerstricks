---
layout: post
title: "Exams"
categories: "MidPack"
date = 2017-11-24T14:17:25+01:00
---

## Exam Registration / Check registered credits 

- go to the [Doctoral Portal](https://student.portal.chalmers.se/doctoralportal/handbook/Doctoral%20degree%20certificate/Pages/default.aspx)
- click on "Login" (written in blue in the greyish submenu bar on the top of the page)
- enter your cid + password 
- click on: "Examination, sign-up/cancel sign up" / "Create transcript of records, postgraduate studies" 

**(important : make sure you are in the Doctoral Portal! If you end up in the Student Portal "your account doesn't have permission to use the service”)**


## Course schedules, room info
In [Time-edit](https://se.timeedit.net/web/chalmers/db1/public/##) you can find information on *when* and *where* a course is given at Chalmers
