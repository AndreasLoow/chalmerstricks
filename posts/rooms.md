---
layout: post
title: "Rooms"
categories: "MidPack"
date = 2017-11-24T14:17:25+01:00
---

## Rooms

To find the location of rooms on campus you can go to [Map (online/app)](http://maps.chalmers.se/) or download the app for smart phones. 


## Booking a room
To book a room you need to create and event in the exchange calendar of that room. 

Log in to [webmail client](https://webmail.chalmers.se)). 

Mark the room (and the other people involved in the meeting) as an **attendee**. 


**procedure to book rooms directly from your calendar app** 
Adding a room to your calendar 'followers' enables you to check the availability of a room and book it directly from your calendar app (e.g. it works smoothly with iCal). 

In order to have (at least in the outlook-chalmers-webmail UI) the calendar of a Chalmers room:

1. Enter the webmail client and go into the **People** tab
2. Under the **Directory** section, (once opened) you find **Chalmers Room**
3. Add the room that you are interested as contacts (I suggest to have all of them into a list of contacts)
4. In the **Calendar** tab, you now can search the room name (**as a person**) and it will appear (It is important to search "as people" otherwise you just search in your calendar events)



# Virtual room
The 'corner' room on the 4th floor has a wonderful conference equipment, with excellent webcam and good audio. 
To book the equipment (unfortunately as of 2017 it is not possible to book the room) you need to: 
1. go to the outlook calendar (after logging in in your email) or your calendar app (when the rooms are added as contact)
2. create a new meeting
3. choose 'EDIT Virtuellt Rum 1' (invite as attendee)
4.  set time, add other attendees and send the email. 

**In the room (local party):** press “wake” (twice), switch the large screen on, click on 'call' meeting virtual room
You can adjust the camera: click on 'move' you will be able to turn and zoom (very useful to show the wihteboard)

**In the browser (remote party):** 
	click on the link which comes with the invitation email (it’s always the same), 
	allow browser to access camera / video. If you use this service a lot, you can download *lifesizecloud* to acces more features. 
