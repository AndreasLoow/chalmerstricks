---
layout: post
title: "Benefits"
categories: "MidPack"
date = 2017-11-24T14:17:25+01:00
---



## Gym & massage expense refund (LRS)

As a Chalmers employee you have the right to be refunded for up to 2000SEK/year for expenses related to health and wellness. 
This is called [Benify](https://www.benify.se/fps/welcomeCustomer.do?nav.id=520)

# How to claim expenses and be refund

	- go to [insidan](http://www.chalmers.se/insidan) 
	- on the right-most column select *Chalmers Plus* in the menu under 'E-tjanst', clic *visa*
	- insert CID and CID-password
	- click on 'wellness receipts'
	- follow instruction on how to register a receipt (useful instructions in english also under 'How do I register my expenses?'
	- print the form and attach your original receipt
	- send it to LRS using an envelop in the post room (put it in the outgoing internal mail pile)

## Chalmers discounts

As a Chalmers employee you can use [Chalmers Plus](http://plus.portal.chalmers.se/) services which guarantee you discounts on several goods and services such as: 
car rentals, travel trolleys, hi-fi devices, Apple computers...


## Mecenat

The [Mecenat](https://mecenat.com/se/om-mecenat/vara-kort) card gives you access to more student discount (than just the DS card). Very useful, keep track of sales with the app!
(Apple, Swebus, Comviq, Tele2, Tre, SF Cinema, Adlibris, ...)