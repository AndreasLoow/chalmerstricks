# PhD useful links (general)
[Handbook for doctoral studies](https://www.chalmers.se/insidan/EN/education-research/doctoral-student/handbook-for-doctoral8636)
Procedure for licentiate Thesis, followup meetings and needed forms

[Doctoralportal](https://student.portal.chalmers.se/doctoralportal/Pages/Doctoralportal.aspx)

[Insidan](https://www.chalmers.se/insidan/EN/)

[Welcome leaflet](http://wiki.portal.chalmers.se/cse/uploads/PhDStudents/welcome_leaflet.pdf) with a bunch of useful organizational information (also for non-PhDs)

[Licentiate degree](http://www.chalmers.se/insidan/sites/cse/doctoral-ed/phd-student/licentiate-degree)

[Follow-up meetings](http://www.chalmers.se/insidan/sites/cse/doctoral-ed/phd-student/follow-up-meetings)


## Travel Approval
[Travel Approval](http://wiki.portal.chalmers.se/cse/pmwiki.php/ST/BusinessTravelApproval) : [e-form](https://docs.google.com/forms/d/e/1FAIpQLSdZDN5LPn2lget_3JQWUJImnJMx5TM7WjClmOJmVl2TIcMoyQ/viewform)

## Expense report and Vacation
[Primula](https://personal.portal.chalmers.se/chalmers/)
[TODO] Procedure of filing expense report (including LRS) 

### How to select meal benefits ###

- For each meal _M_ on each day there are three options:
    1. Checking box **with meal benefits** (**B**): choose this option if meal _M_ is included as part of another fee (for example, a lunch that is included in a conference registration fee) **and** _M_ is not a breakfast.
    2. Checking box **without meal benefits** (**N**): choose this optin if meal _M_ is offered by a third party (for example, if you are a guest and your host pays for _M_) **or** _M_ is a breakfast (which is usually included in hotel fee)
    3. Not checking either box (**E**): choose this option if you pay for meal _M_ yourself (for example, you go out to a restaurant and pay your share of the bill out of your own pocket) (Don't clicking on anything)

The consequences of these options are:

1. **B** (with meal benefits) implies that you will not get an allowance for meal _M_, but Chalmers paid indirectly (for example through the registration fee) and will pay taxes accordingly.
2. **N** (without meal benefits) implies that you will not get an allowance for meal _M_, and Chalmers did not pay either.
3. **E** (not clicking on anything) implies that you will get an allowance for meal _M_. Note, however, that the allowance is a fixed amount: you do not get reimbursed what you paid exactly (and hence you do not need to keep receipts of meal expenses).

## Book a trip via egencia online
[Register](http://go.egencia.com/R00y00fT0X003000FrYu6S0)
The first attempt for registration usually fails :), don't get disappointed, try again

[Login](http://chalmers.egencia.se/)
A dedicated login page (which also implies you wouldn't be able to login using the public Engencia page) 

[Booking](http://chalmers.egencia.se/app?service=external&page=Login&mode=form&market=SE)
Email address to ask questions or order via email: Team3.got@viaegencia.com

## Gym & massage expense refund (LRS)
[Chalmers Plus](http://plus.portal.chalmers.se/)

[Benify](https://www.benify.se/fps/welcomeCustomer.do?nav.id=520)

## List of passed courses and certificate
[Student portal](https://student.portal.chalmers.se/en/chalmersstudies/Pages/services.aspx)
[Exam Registration](https://student.portal.chalmers.se/doctoralportal/gts/news/Pages/Dotoral-Student-Exam-Registration.aspx)

## Course schedules, room info
[Time-edit](https://se.timeedit.net/web/chalmers/db1/public/##)

# Rooms
[Map (online/app)](http://maps.chalmers.se/) To find rooms

## Booking Rooms
** To book a room you need to create and event in the exchange calendar (after loggining to [webmail client](https://webmail.chalmers.se)) and mark the room as an attendee.  **


## Room Availability
In order to have (at least in the outlook-chalmers-webmail UI) the calendar of a Chalmers room:

1. Enter the webmail client and go into the **People** tab
2. Under the **Directory** section, (once opened) you find **Chalmers Room**
3. Add the room that you are interested as contacts (I suggest to have all of them into a list of contacts)
4. In the **Calendar** tab, you now can search the room name (**as a person**) and it will appear (It is important to search "as people" otherwise you just search in your calendar events)



# Student union
[Paying for student uninon card](http://medlem.chs.chalmers.se)
** Student union card is REQUIRED for taking an exam **
While you can delay the payment of student uninon membership fee (meaning that you wouldn't get card as well), it is encouraged to pay the membership fee since the uninon card actually provides you additional benefits such as discounts and so on. 


## Discount (Apple, Swebus, Comviq, Tele2, Tre, SF Cinema, Adlibris, ...)
[Mecent](https://mecenat.com/se/) SJ and vastraffik do have discount with student union card even if you are above 26.
Bus 16 and 55 (Main stations: Chalmers, Chalmers Tvärgatan - Brunsparken - Lindholem) are also available for 300 SEK (6 months period card)

You can install an app to browse through discounts.

** If you have teaching in Lindholmen you can get this card for free **

# Books
[Request material(books, articles)](http://www.lib.chalmers.se/en/search/purchase-suggestions-and-interlibrary-loans/)
You can request/recommand a (paper/online) book for library. (the book doesn't necessary need to be relavant to your research area).
Books : 1-3 weeks -  E-book, articles: few days.

# Computer/Internet

[CDKS](http://cdks.chalmers.se) Change CID, NOMAD password


# Wireless eduroam
There is an installer in [eduroam website](https://cat.eduroam.org/) (even for Linux) that embedded the eduroam certificate.

## Manual setup
- Login ID: CID@chalmers.se
- Anonymous ID: empty
- Password: CID password
- Authentication: MSCHAPv2
- EAP type: PEAP
- Eduroam certificate is in eduroam.pem


# Free commercial softwares
Worth looking (OS and utitily software(antivirus) and domain specific softwares like MATLAB and ...)
Windows share:

- smb://syros.ce.chalmers.se
- \\\\syros.ce.chalmers.se

- Domain: NET
- username: CID

[syros directory tree](syrosdirtreed3.txt) output of "tree -L 3 -d" on smb://syros.ce.chalmers.se/program/

## Microsoft tools
Different version of Windows (even ancient DOS), SQL server and visual studio and more. You need to specify your university (Chalmers).

[Microsoft tools](https://onthehub.com/microsoftimagine) Almost everything except MS-Office which is accessable from SYROS server.


## Windows servers via remote desktop
- rdesktop remote.cda.chalmers.se
- login name: NET\CID or CID@net.chalmers.se

This is specially useful for using Microsoft office (Word, powerpoint, Silverlight and IE!!)

## Mountable file systems

### Home directory of remote servers
sftp://CID@remote12.chalmers.se/chalmers/users/CID

### Course directory
- /chalmers/groups/edu2009/www/www.cse.chalmers.se/year/2015/course/TDA352_Cryptography
- accessable :  http://www.cse.chalmers.se/edu/course/TDA352_Cryptography/
- [Simple sync script](sync.sh)

### Windows home directoory:
file00.chalmers.se  (accessable via smb://HOST or \\\\HOST)

### Linux home directory
sol.ita.chalmers.se (accessable via smb://HOST or \\\\HOST)
[More info](https://student.portal.chalmers.se/en/contactservice/ITServices/self-administered/linux/file-storage/Sidor/Mount-home-directory.aspx)

## Chalmers provide
- VPN accountes
- SVN accountes
- mysql accountes
- oracle accountes

# Email clients

## SMTP
- smtp.chalmers.se : 587
- username : CID
- Auth method: NTLM
- Connection: STARTTLS

## IMAP
- imap.chalmers.se : 993
- username : CID
- Auth method: Normal password
- Connection: SSL/TLS
// Note : Thunderbird test doesn't work correctly.

## iOS
[Detail setting from IT portal](https://it.portal.chalmers.se/itportal/MobilSurfPlattaApple/Epost)
- Select Exchange account
- On the first page, use 'CID@chalmers.se'
- Domain: NET
- Server: webmail.chalmers.se
// Note: if you found some information page from Chalmers website saying the server "m.outlook.com", that is intended for Chalmers student

## Thunderbird/Outlook/Exchange integration
Tools -> addon -> "Lightning"

[Exchange calendar](https://github.com/Ericsson/exchangecalendar/releases) Get direct access to Chalmers ldap contacts in thunderbird using this extension

# Free commercial services
- [Sharelatex](https://sharelatex.com/)
- [Bitbucket](http://bitbucket.com)
- [Github](http://github.com)
- [Box](http://box.com)
- [Prezi](http://prezi.com)

## Web space
- /chalmers/users/CID/www/www.cse.chalmers.se
- http://www.cse.chalmers.se/~CID/

### Permissions
Sometimes after editing a file in your web directory if will become inaccessible from the web.
This script applies the right permissions to everything.
Run from within your `www` directory.

```bash
#!/bin/bash
CONF=/chalmers/sw/sup64/fix_personal_www_rights-1.1/etc
find www.cse.chalmers.se -type d                -print0 | xargs -r -0 nfs4_setfacl -S ${CONF}/dir.acl
find www.cse.chalmers.se -type f   -perm /ugo+x -print0 | xargs -r -0 nfs4_setfacl -S ${CONF}/xfile.acl
find www.cse.chalmers.se -type f ! -perm /ugo+x -print0 | xargs -r -0 nfs4_setfacl -S ${CONF}/file.acl
```

(Source: Patrick Forsberg, private email 2013-12-18)



### Calendar
In Sweden knowing week numbers is really useful (Google calendar support showing week # in the calendar view).
In Ubuntu to show calendar with week numbers (or set it in "time & calendar" indicator) :
```
gsettings set org.gnome.shell.calendar show-weekdate true
```

# Swedish
[Folkuniversitetet](http://www.folkuniversitetet.se/chalmers) Only basic courses are free

[GU - Swedish language and culture](http://uf.gu.se/english/ask/staff/language-courses/?languageId=100001) course for international staff, guest researchers and PhD students

# Union
[Akademikernas a-kassa](https://www.aea.se/) at least 12 months before graduation.

[Why](https://www.thelocal.se/20160921/7-reasons-you-should-join-swedens-a-kassa-akadermikernas-akassa-tlccu)

# Fire

For each iteration of a course (using Fire) a new Fire instance is needed. To create a new Fire instance mail Evgeny Kotelnikov and provide:

 - The course name
 - All course codes (courses given at both Chalmers and GU has at least two)

For each new instance you need to add the actual labs. Note the difference between "group labs" and "individual labs".

Then you need to add graders under the "Graders" tab. Make sure that the grading workload (the lower table) is filled out correctly (otherwise students will not be able to submit assignments).

Some things are not possible to do using the UI, such as removing labs. For this you need to mail Evgeny.


## PhD students' email addresses
[PhD-Emaillist.txt](PhD-Emaillist.txt)


## Template for answering interested students (PhD level)
[templateEmail.txt](templateEmail.txt)

## Working remotely via SSH servers

VPN, SSH can be used to work remotely while keeping your access to chalmers resources, restricted articles and libraries.

[self-administered](https://student.portal.chalmers.se/en/contactservice/ITServices/self-administered/Sidor/default.aspx)

- ssh CID@remote12.chalmers.se
- ssh CID@remote2.student.chalmers.se
- ssh CID@remote11.chalmers.se

If you do not want to reenter your password all the time, add
the following two lines to the corresponding entries in `$HOME/.ssh/config`:

~~~~
     GSSAPIAuthentication yes
     GSSAPIDelegateCredentials yes
~~~~

After that, having a valid Kerberos ticket will suffice to log in.

A more lightweight solution is to use sshuttle (https://github.com/apenwarr/sshuttle).
Running the following command will automatically tunnel all TCP connections and
DNS requests through the chalmers network:
~~~~
sshuttle -r remote11.chalmers.se 0/0
~~~~


# Printer
[Web interface to printers](https://print.chalmers.se/auth/uploadme.cgi)
You can use web interface to see nearby printers, download drivers or directly send pdf, txt files to be printed.

## Ubuntu installation (tested on 14.x, 12.x)
[Download Drivers](http://print.chalmers.se/drivers/)

- in "system-config-printer" type  smb://print.chalmers.se/cse-ed-5473-laser1 (or your printer address)
- add corresponding driver file (*.PDD). It fails to authenticate first, but you can set the password here when you look at printer queue.
- username: NET/CID

## Printing via ipp/kerberos:
Copy `krb5.conf` in this repository to `/etc/krb5.conf` and
run:
~~~~
test -d $HOME/.cups || mkdir $HOME/.cups
cat <<EOF >$HOME/.cups/client.conf
ServerName print.chalmers.se
GSSServiceName HTTP
EOF
~~~~
In older versions of CUPS, you may have to add those lines to the system-wide configuration file in `/etc/cups/client.conf`.
Then, printing should work whenever you have a valid Kerberos ticket that
can be obtained by running `kinit $YOUR_CID`




## Sign up for exams / Check registered credits 

- go to the [Doctoral Portal](https://student.portal.chalmers.se/doctoralportal/handbook/Doctoral%20degree%20certificate/Pages/default.aspx)
- click on "Login" (written in blue in the greyish submenu bar on the top of the page)
- enter your cid + password 
- click on: "Examination, sign-up/cancel sign up" / "Create transcript of records, postgraduate studies" 

**(important : make sure you are in the Doctoral Portal! If you end up in the Student Portal "your account doesn't have permission to use the service”)**


--

- Hamid Ebadi
- Daniel Schoepe
